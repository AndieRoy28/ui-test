﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Switch.Json;
using Switch.Json.Linq;
using SwitchBatchManagerCommons.Models;
using StreamsScheduleTask.Models;
using System.IO;
using ScheduleUtil.Models;
using ScheduleUtil;
using Switch.Blob;
using Switch.Time;
using Switch.Queue;
using System.Net;
using SelectPdf;
using System.Diagnostics;

namespace StreamsScheduleTask
{
    /*
     * Steps to Create a Batch Task Manager Application Pair
     * 1. Rename the ITaskWorker Class with either 
     *          If this Task is part of a Batch Manager Project then use Manager Name -> [uniqueName]
     *          If this Task is an independant one, then select a Unique Name
     * 2. When you call the OnRunTask, the Co-Ordinator will be looking for an assocated dll contained in a folder of the same name
     *          data-exchange/switch-batch/realtime/[uniqueName]
     * 3. Remove the Sample Code when you build your project
     * 4. You will also need to use the OnTaskReport event regularly to report progress of your app
     * 5. Switch the Output Type of the Project Depending on the following:
     *          For Local Debug - OutputType = Console Application
     *          For Deployment  - Output Type = Class Library
     */

    public class StreamsScheduleTaskWorker : ITaskWorker
    {
        public event EventHandler<LogArgs> OnLog;
        public event EventHandler<TaskReportArgs> OnTaskReport;
        private string _sourceId;
        private Guid _taskId;

        private SwitchQueue<Notification> _emailQueue = null;
        private string _queueName = "switch-notification";

        private SwitchQueue<SAToCsv> _csvQueue = null;
        private string _queueNameCSV = "sa-csv-export";

        //private string _webUrl = "http://localhost:8080";
        private string _webUrl = "https://hub3.switchautomation.com";

        private string _conStringSwitchContainer = "DefaultEndpointsProtocol=https;AccountName=switchcontainer;AccountKey=rIlipuK7mA2/AaXO2J17bZjakY8q3GHzkfXs0CeEZmQjnqpE5jDkYGKbrQmBY6Fse2pA2oaHrl2iNs7XwLnxcw==";
        private string _conStringSwitchHub = "DefaultEndpointsProtocol=https;AccountName=switchhub;AccountKey=Lm742wFoaTjZAcw9ESJUMUTuO60OPwLjw1YBv0HAwI2hhJ7yUc2zk3gTqcCi8AQ66DE8ErCZ9QslDjOU90bFrg==";
        private string _appname;
        public void Start(string appName, Guid taskId, JObject args)
        {
            try
            {
                _appname = appName;
                if (_emailQueue == null)
                {
                    _emailQueue = new SwitchQueue<Notification>(_conStringSwitchContainer, _queueName);
                }
                _sourceId = appName + "_ " + taskId.ToString();
                _taskId = taskId;

                var jobArgs = JsonConvert.DeserializeObject<JobArgs>(args.ToString());
                CheckJobArgs(jobArgs, appName);
                StartLog();

                // Do your Processing here
                for (int jobCount = 0; jobCount < jobArgs.JobsList.Count(); jobCount++)
                {
                    var job = jobArgs.JobsList[jobCount];
                    job.SAUrl = string.Format("{0}/#insights/site-analysis/i:{1}/sid:{2}/co:true", _webUrl, job.InstallationID, job.SettingsID);

                    OnLog(this, new LogArgs(_sourceId, MethodBase.GetCurrentMethod().Name,
                        new List<LogMessage>
                        {
                            new LogBatchTask() {
                                Message = "\nProcessing...\n\tUserID:" + job.UserID + "\n\tInstallationID: " + job.InstallationID + "\n\tSheduleID: " + job.ScheduleID + "\n\tStreamType: " + job.StreamType,
                                JobNumber = jobCount, JobName = job.ScheduleID }
                        }));

                    OnTaskReport(this, new TaskReportArgs(_taskId, "Started " + job, jobArgs.JobsList.Count(), (jobCount + 1), 0));
                    OnTaskReport(this, new TaskReportArgs(_taskId, "Started ScheduleId: " + job.ScheduleID, jobArgs.JobsList.Count(), (jobCount + 1), 0));

                    var isStreamCreated = false;
                    switch (job.StreamType)
                    {
                        case "PDF":
                            isStreamCreated = PDFTask(job);
                            break;
                        case "Image":
                            isStreamCreated = ImageTask(job);
                            break;
                        case "CSV":
                            isStreamCreated = CSVTask(job);
                            break;
                        case "CHECK":
                            //CheckRunDate(job, jobCount);
                            break;
                    }

                    if (isStreamCreated)
                    {
                        //delete completed job
                        StreamsJobUtil.DeleteStreamsSchedule(job.PartitionKey, job.RowKey);
                        StreamsJobUtil.DeleteStreamsScheduleJob(job.ScheduleID, job.RowKey);

                        var nextRunTick = Convert.ToInt64(job.NextRun);
                        var rowKeyTick = Convert.ToInt64(job.RowKey);
                        if (nextRunTick > 0)
                        {
                            var nextRun = new DateTime(rowKeyTick).AddTicks(nextRunTick);
                            var p = job.SettingsJSON.Split(new char[] { ',' });

                            if (job.Frequency == 1)
                            {
                                var count = Convert.ToInt32(p[0]);
                                var m = nextRun.AddMonths(count);
                                var nextNewRun = ScheduleHelper.GetMonthInDay(m, Convert.ToInt32(p[2]), count, Convert.ToInt32(p[1]));
                                job.NextRun = (nextNewRun.Ticks - nextRun.Ticks).ToString();
                            }

                            if (job.Frequency == 2)
                            {
                                var nextNewRun = ScheduleHelper.ComputeRunONDate(Convert.ToInt32(p[0]), nextRun, Convert.ToInt32(p[1]), p[2]);
                                job.NextRun = (nextNewRun.Ticks - nextRun.Ticks).ToString();
                            }

                            if (job.StreamType == "CSV")
                            {
                                var decodeCSV = Convert.FromBase64String(job.CsvParam);
                                var csvParam = JsonConvert.DeserializeObject<SAToCsv>(Encoding.UTF8.GetString(decodeCSV));

                                var dateIncrement = nextRun.Ticks - (new DateTime(rowKeyTick)).Ticks;
                                var nextRangeTo = Convert.ToDateTime(csvParam.RangeTo).AddTicks(dateIncrement);

                                csvParam.RangeTo = nextRangeTo.ToString();
                                var _csvparam = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(csvParam));
                                job.CsvParam = Convert.ToBase64String(_csvparam);
                            }

                            var pKey = Guid.NewGuid().ToString();
                            StreamsJobUtil.SaveStreamsScheduleJob(new StreamsScheduleJob(job.ScheduleID.ToString(), nextRun.Ticks.ToString(), pKey));
                            StreamsJobUtil.SaveScheduleSetting(new StreamsSchedule(pKey, nextRun.Ticks.ToString(), job.InstallationID, job.SettingsID, job.UserID, job.SettingsJSON, job.ScheduleID, job.NextRun, "0", job.Frequency, job.SettingsTitle, job.TemplateId, job.StreamType, job.CsvParam));
                        }
                        
                    }
                    else
                    {
                        StreamsJobUtil.SaveScheduleSetting(new StreamsSchedule(job.PartitionKey, job.RowKey, job.InstallationID, job.SettingsID, job.UserID, job.SettingsJSON, job.ScheduleID, job.NextRun, "0", job.Frequency, job.SettingsTitle, job.TemplateId, job.StreamType, job.CsvParam));
                    }

                    OnTaskReport(this, new TaskReportArgs(_taskId, "Completed ScheduleId: " + job, jobArgs.JobsList.Count(), (jobCount + 1), 100));
                }

                OnTaskReport(this, new TaskReportArgs(_taskId, "Jobs Complete"));
            }
            catch (Exception exception)
            {
                OnLog(this, new LogArgs(_sourceId, MethodBase.GetCurrentMethod().Name,
                   new List<LogMessage> { new LogException() { Message = "Start Exeption: " + exception.ToString() } }));
            }
            finally
            {
                Thread.Sleep(1000);
            }
        }

        public void CheckJobArgs(JobArgs jobArgs, string appName)
        {
            if (jobArgs == null)
            {
                OnLog(this, new LogArgs(appName, MethodBase.GetCurrentMethod().Name,
                    new List<LogMessage>
                    {
                            new LogException() {Message = "Could not Deserialize Task Arguments"}
                    }));
                return;
            }
        }

        public void StartLog()
        {
            OnLog(this, new LogArgs(_appname, MethodBase.GetCurrentMethod().Name,
                new List<LogMessage>
                {
                    new LogBatchTask
                    {
                        Message = "Starting task"
                    }
                }));
        }

        public bool CSVTask(StreamsSchedule job)
        {
            var result = false;
            if (_csvQueue == null)
            {
                _csvQueue = new SwitchQueue<SAToCsv>(_conStringSwitchContainer, _queueNameCSV);
            }

            if (!_csvQueue.CheckExistsQueue(_queueNameCSV))
                _csvQueue.CreateQueue(_queueNameCSV);

            var decodeCSV = Convert.FromBase64String(job.CsvParam);
            var csvParam = JsonConvert.DeserializeObject<SAToCsv>(Encoding.UTF8.GetString(decodeCSV));

            csvParam.InstallationId = job.InstallationID;
            csvParam.UserEmail = ScheduleHelper.GetUserEmail(job.UserID.ToString(), job.SettingsID);

            result = _csvQueue.Send(csvParam, _queueNameCSV);

            return result;
        }

        private bool ImageTask(StreamsSchedule job)
        {
            OnLog(this, new LogArgs(_sourceId, MethodBase.GetCurrentMethod().Name,
                        new List<LogMessage>
                        {
                            new LogBatchTask() {
                                Message = "Image Task",
                                JobName = job.ScheduleID }
                        }));
            var result = false;
            var ext = "png";
            if (ImageCapture(job, Guid.NewGuid().ToString(), ext))
            {
                //TODO send email
                var body = ScheduleHelper.GetHtmlBody(string.Format("{4}/{0}/{1}/{2}/{3}.{4}", job.UserID, job.InstallationID, job.ScheduleID, job.RowKey, ext));
                result = SendEmail(job, body);
            }

            return result;
        }

        private bool PDFTask(StreamsSchedule job)
        {
            OnLog(this, new LogArgs(_sourceId, MethodBase.GetCurrentMethod().Name,
                        new List<LogMessage>
                        {
                            new LogBatchTask() {
                                Message = "PDF Task",
                                JobName = job.ScheduleID }
                        }));
            var result = false;

            var ext = "pdf";
            if (ImageCapture(job, Guid.NewGuid().ToString(), "png"))
            {
                string htmlString = ScheduleHelper.GetHtmlPDF(string.Format("png/{0}/{1}/{2}/{3}.png", job.UserID, job.InstallationID, job.ScheduleID, job.RowKey));
                HtmlToPdf converter = new HtmlToPdf();
                PdfDocument doc = converter.ConvertHtmlString(htmlString);
                var b = doc.Save();
                doc.Close();

                if (BlobHelper.PutBlob(_conStringSwitchHub, b, string.Format("hub-assets/streams-schedule/{4}/{0}/{1}/{2}/{3}.{4}", job.UserID, job.InstallationID, job.ScheduleID, job.RowKey, ext), "static"))
                {
                    //TODO send email
                    var body = ScheduleHelper.GetHtmlBodyLink(string.Format("{4}/{0}/{1}/{2}/{3}.{4}", job.UserID, job.InstallationID, job.ScheduleID, job.RowKey, ext));
                    result = SendEmail(job, body);
                }
            }

            return result;
        }

        private void CheckRunDate(StreamsSchedule s, int jobCount)
        {
            var job = s;
            Console.WriteLine("\nRun Date:" + new DateTime(Convert.ToInt64(job.RowKey)) + "\n");


            for (int i = 0; i < 12; i++)
            {
                var nextRun = new DateTime(Convert.ToInt64(job.RowKey)).AddTicks(Convert.ToInt64(job.NextRun));
                var p = job.SettingsJSON.Split(new char[] { ',' });
                if (job.Frequency == 1)
                {
                    var count = Convert.ToInt32(p[0]);
                    var m = nextRun.AddMonths(count);
                    var nextNewRun = ScheduleHelper.GetMonthInDay(m, Convert.ToInt32(p[2]), count, Convert.ToInt32(p[1]));
                    job.NextRun = (nextNewRun.Ticks - nextRun.Ticks).ToString();
                }

                if (job.Frequency == 2)
                {
                    var nextNewRun = ScheduleHelper.ComputeRunONDate(Convert.ToInt32(p[0]), nextRun, Convert.ToInt32(p[1]), p[2]);
                    job.NextRun = (nextNewRun.Ticks - nextRun.Ticks).ToString();
                }

                Console.WriteLine("Next Run Date:" + nextRun);
                job.RowKey = nextRun.Ticks.ToString();
            }
        }

        private bool SendEmail(StreamsSchedule job, string body)
        {
            var emailAdress = ScheduleHelper.GetEmailAddresses(job.UserID.ToString(), job.SettingsID);

            if (!_emailQueue.CheckExistsQueue(_queueName))
                _emailQueue.CreateQueue(_queueName);

            var subject = string.Format("{0} - Scheduled Report", job.SettingsTitle);
            body = body.Replace("<SUBJECT>", job.SettingsTitle);
            var template = ScheduleHelper.GetEmailTemplateByWhiteLabelId(job.TemplateId);
            template = template.Replace("{SUBJECT}", subject).Replace("{MESSAGE}", body);
            var n = new Notification()
            {
                Message = template,
                Subject = subject,
                Recipients = JsonConvert.SerializeObject(emailAdress)
            };

            return _emailQueue.Send(n, _queueName);
        }

        public bool ImageCapture(StreamsSchedule job, string filename, string ext)
        {
            try
            {
                var path2 = "";

                var directoryInfo = new DirectoryInfo(Environment.CurrentDirectory).Parent;
                if (directoryInfo != null && directoryInfo.Parent != null && directoryInfo.Parent.Parent != null)
                {
                    var rootFolder = directoryInfo.Parent.Parent;
                    var workerExes = Directory.EnumerateFiles(rootFolder.FullName, "*.exe", SearchOption.AllDirectories).ToList();
                    foreach (var workerExe in workerExes)
                    {
                        if (workerExe.Contains("phantomjs"))
                        {
                            path2 = workerExe;
                        }
                    }
                }

                var loginURL = string.Format("{0}/v1/passthrough/290491D9CA40/?user={1}", _webUrl, job.UserID);
                var logoutURL = string.Format("{0}/#logout", _webUrl, job.UserID);
                var homeURL = string.Format("{0}/#hub/co:true", _webUrl);
                //var jsScriptURL = "http://az412604.vo.msecnd.net/static/hub-assets/streams-schedule/js/site-analysis-chart.js";
                var assetDirectory = string.Format("{0}{1}", new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName, "\\assets");
                var jsDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);// string.Format("{0}{1}", new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName, "\\assets"); // new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName;//string.Format("{0}{1}", new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.Parent.FullName, "\\StreamsScheduleTask");
                var process = new Process();
                var startInfo = new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    FileName = path2,
                    Arguments = string.Format("\"{0}\\{1}\" {2} {3} {4} {5} {6}", jsDirectory, "site-analysis-chart.js", job.UserID, job.SAUrl, filename, loginURL, homeURL, logoutURL)
                };


                process.StartInfo = startInfo;
                process.Start();
                string output = process.StandardOutput.ReadToEnd();
                process.WaitForExit();

                return BlobHelper.PutBlob(_conStringSwitchHub, Convert.FromBase64String(output), string.Format("hub-assets/streams-schedule/{4}/{0}/{1}/{2}/{3}.{4}", job.UserID, job.InstallationID, job.ScheduleID, job.RowKey, ext), "static");
            }
            catch (Exception ex)
            {
                //var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                //var path2 = "";
                //var path3 = new DirectoryInfo(Environment.CurrentDirectory).Parent.FullName;
                //var path4 = string.Format("{0}{1}", new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName, "\\assets");
                //var directoryInfo = new DirectoryInfo(Environment.CurrentDirectory).Parent;
                //if (directoryInfo != null && directoryInfo.Parent != null && directoryInfo.Parent.Parent != null)
                //{
                //    var rootFolder = directoryInfo.Parent.Parent;
                //    var workerExes = Directory.EnumerateFiles(rootFolder.FullName, "*.exe", SearchOption.AllDirectories).ToList();
                //    foreach (var workerExe in workerExes)
                //    {
                //        if (workerExe.Contains("phantomjs"))
                //        {
                //            path2 = workerExe;
                //        }
                //    }
                //}


                OnLog(this, new LogArgs(_sourceId, MethodBase.GetCurrentMethod().Name,
                        new List<LogMessage>
                        {
                            new LogBatchTask() {
                                Message = ex.ToString(),// + "\nPath: " + path + "\nPath2: " + path2 + "\nPath3: " + path3 + "\nPath4: " + path4,
                                JobName = job.ScheduleID }
                        }));
                return false;
            }
        }
        
    }
}
