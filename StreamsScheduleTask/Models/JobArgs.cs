﻿using ScheduleUtil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamsScheduleTask.Models
{
    public class JobArgs
    {
        public JobArgs()
        {
            JobsList = new List<StreamsSchedule>();
        }

        public List<StreamsSchedule> JobsList { get; set; }
    }
}
