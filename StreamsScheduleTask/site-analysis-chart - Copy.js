var page = require('webpage').create();
var system = require('system');
page.viewportSize = { width: 1820, height: 900 };

var timeoutId = 0;
var waitCounter = 0;

var fs = null;
var path = '';
var content = '';

try {
	fs = require('fs');
	path = 'output.txt';
}
catch(err) {
	console.log(err);
}

// system.args[4] - login
page.open(system.args[4], function (status) {
    if (status !== 'success') {
        console.log('Unable to access network');
        phantom.exit();
    }
    else {
        page.render("../../images/ScreenCapture/result/_0.png");

        content = 'Login SUCCESS';
        if (fs != null) {
            fs.write(path, content, 'w');
            fs.write(path, system.args[2], 'w');
        }
        // system.args[2], - sa url
		page.open(system.args[2], function (status) {
		    page.render("../../images/ScreenCapture/result/_1.png");
            if (status == 'success') {
                page.render("../../images/ScreenCapture/result/_2.png");
                content = 'SA loaded';
				if (fs != null)
                fs.write(path, content, 'a/+');

				waitCounter = 0;
                // system.args[3] - filename
                timeoutId = setTimeout(waitForChart, 10000, page, system.args[3]);
            }
            else {
                console.log('Unable to access network: ' + system.args[3]);
                phantom.exit();
            }
        });
    }
});

function waitForChart(page, filename) {
    if (isChartReady()) {
        content = 'SA ready';
		if (fs != null)
        fs.write(path, content, 'a/+');
        captureChart(page, filename);
    }
    else {
        content = 'waiting for SA...';
		if (fs != null)
        fs.write(path, content, 'a/+');

        waitCounter++;
        console.log('waitForChart: ' + waitCounter);
        page.render("../../images/ScreenCapture/result/" + filename + "_" + waitCounter + ".png");

        if (timeoutId)
            clearTimeout(timeoutId);
        timeoutId = setTimeout(waitForChart, 10000, page, filename);
    }
}

//function isChartReady() {
//    if ($('#site-data-analysis-container').html().length > 0) {
//        return true;
//    }
//    else {
//        return false;
//    }
//}

//function captureChart(page, filename) {

//    content = 'Capturing SA...';
//	if (fs != null)
//    fs.write(path, content, 'a/+');

//    var clipRect = page.evaluate(function () {
//        return document.querySelector('#site-data-analysis-container').getBoundingClientRect();
//    });
//    page.clipRect = {
//        top: clipRect.top,
//        left: clipRect.left,
//        width: clipRect.width,
//        height: clipRect.height
//    };
//    console.log('true');
//    page.render("../../images/ScreenCapture/result/" + filename + ".png");

//    content = 'Phantom Exit..';
//	if (fs != null)
//    fs.write(path, content, 'a/+');

//    phantom.exit();
//}