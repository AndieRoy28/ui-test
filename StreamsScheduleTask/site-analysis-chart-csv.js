var page = require('webpage').create(), system = require('system'), fs = require('fs');

var timeoutId = 0;
var waitCounter = 0;
var path = '';
var content = '';

page.viewportSize = { width: 1820, height: 900 };
page.open(system.args[3], function (status) {
    if (status !== 'success') {
        pageUnsuccess();
    }
    else {
        page.render("../../images/db/1_.png");
        //setTimeout(function () {
        //    page.open(system.args[4], function (status2) {
        //        if (status2 !== 'success') {
        //            pageUnsuccess();
        //        }
        //        else {
        //            setTimeout(function () {
        //                page.open(system.args[2], function (status3) {
        //                    if (status3 == 'success') {
        //                        pageSiteAnalysis();
        //                    }
        //                    else {
        //                        pageUnsuccess();
        //                    }
        //                });
        //            }, 5000);
        //        }
        //    });
        //}, 2000);

        setTimeout(function () {
            page.open(system.args[2], function (status3) {
                if (status3 == 'success') {
                    page.render("../../images/db/2_.png");
                }
                else {
                    pageUnsuccess();
                }
            });
        }, 5000);
    }
});

function pageLogout() {
    page.open(system.args[5]);
}

function pageUnsuccess() {
    console.log('Unable to access network');
    phantom.exit();
}

//CSVEXPORTAUTOGENERATE
page.onConsoleMessage = function (msg, lineNum, sourceId) {
    if (msg == 'CSVEXPORTAUTOGENERATE') {
        console.log(true);
        pageLogout();
        phantom.exit();
    }
};