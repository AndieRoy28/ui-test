﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Switch.Blob;
using Switch.Json;
using Switch.Json.Linq;
using Switch.Table;
using SwitchBatchManagerCommons.Models;
using SwitchBatchManagerCommons.Utils;
using StreamsScheduleTask.Models;
using LogArgs = SwitchBatchManagerCommons.Models.LogArgs;
using ScheduleUtil;

namespace StreamsScheduleTask.Test
{
    public class RunManager
    {
        private const string PartitionKey = "18e77313-7534-4261-928c-450f3b7fdb9c";
        private const string BlobContainer = "data-exchange";
        public static string SwitchContainerConstring = "DefaultEndpointsProtocol=https;AccountName=switchcontainer;AccountKey=2NaOVuS/zi0LV3WpdtmvXqJuoetuCdLaKYBT7PkmYM7T23ZVQegWzcnK9En9uXXNhDnF4F8seI2NRZjyyJBslA==";

        private static Timer _sendLogsTimer;
        private static Timer _clearDeletionsTimer;
        private static ITaskWorker _worker;
        private static string _appName;
        private static TaskReport _taskReport;
        private static List<LogArgs> _logsCache;
        private static readonly object _logsCacheLock = new object();

        public static void Main(string[] args)
        {
            var taskManagerWorker = (from type in Assembly.GetExecutingAssembly().GetTypes() let myInterfaceType = typeof(ITaskWorker) where type != myInterfaceType && myInterfaceType.IsAssignableFrom(type) select type).FirstOrDefault();
            if (taskManagerWorker != null)
            {
                _worker = (ITaskWorker)Activator.CreateInstance(taskManagerWorker);
                _appName = _worker.GetType().Name;
                SwitchBatchHelper.AppName = _appName;

                _logsCache = new List<LogArgs>();
                _sendLogsTimer = new Timer(SendLogsCallback, null, TimeSpan.FromSeconds(30), Timeout.InfiniteTimeSpan);

                _worker.OnTaskReport += WorkerOnTaskReport;
                _worker.OnLog += WorkerOnLog;

                //var jobArgs = new JobArgs { JobsList = { "Job 1", "Job 2", "Job 3" } };
                var jobArgs = new JobArgs { JobsList =  StreamsJobUtil.GetStreamsSchedule() };
                _worker.Start("AppName", Guid.NewGuid(), JObject.Parse(JsonConvert.SerializeObject(jobArgs)));
                SendLogsCallback(null);
            }
        }

        static void WorkerOnTaskReport(object sender, TaskReportArgs e)
        {
            if (_taskReport == null)
            {
                var processId = Environment.TickCount.ToString();
                _taskReport = new TaskReport
                {
                    RowKey = _appName + "_" + e.TaskId,
                    AppName = _appName,
                    ProcessId = processId,
                    TaskId = e.TaskId,
                    StatusMessage = "Init"
                };
                _taskReport.StatusMessage = e.StatusMessage;
                _taskReport.LastNotification = DateTime.UtcNow;

                if (e.CurrentJob > -1)
                {
                    _taskReport.CurrentJob = e.CurrentJob;
                }
                if (e.JobCount > -1)
                {
                    _taskReport.JobCount = e.JobCount;
                }
                if (e.PercentComplete > -1)
                {
                    _taskReport.PercentComplete = e.PercentComplete;
                }
            }
            if (!TableStorageRestHelper.InsertOrReplaceEntity(SwitchContainerConstring, "batchcommunications", PartitionKey, _taskReport.RowKey,
                    _taskReport))
            {
                Console.WriteLine("Could not Write Task to TS");
            }
        }

        static void WorkerOnLog(object sender, LogArgs e)
        {
            lock (_logsCacheLock)
            {
                e.DateTime = DateTime.UtcNow;
                e.AppName = _appName + "_0001";
                _logsCache.Add(e);
            }
            foreach (var logMessage in e.LogMessages)
            {
                Console.WriteLine(e.SourceId + " - " + e.MethodName + " - " + logMessage.LogType + " - " + logMessage.Message);
            }
        }

        static void SendLogsCallback(object e)
        {
            if (_logsCache.Any())
            {
                lock (_logsCacheLock)
                {
                    var logsCacheString = JsonConvert.SerializeObject(_logsCache);
                    var encoder = new ASCIIEncoding();
                    var logsCacheBytes = encoder.GetBytes(logsCacheString);
                    var blobName = "logs-ingestion/inbox/" + Guid.NewGuid().ToString() + ".json";
                    BlobHelper.PutBlob(SwitchContainerConstring, logsCacheBytes, blobName, BlobContainer);
                    _logsCache.Clear();
                }
            }

            _sendLogsTimer.Change(TimeSpan.FromSeconds(20), Timeout.InfiniteTimeSpan);
        }
    }
}
