var page = require('webpage').create(), system = require('system');
page.viewportSize = { width: 1820, height: 900 };
page.open(system.args[4], function (status) {
    if (status !== 'success') {
        console.log('Unable to access network');
        phantom.exit();
    }
    else {        
        setTimeout(function () {
            page.render("../../images/db/login" + system.args[3] + ".png");
            page.open(system.args[5], function (status2) {
                if (status2 !== 'success') {
                    console.log('Unable to access network');
                    phantom.exit();
                }
                else {
                    page.render("../../images/db/home" + system.args[3] + ".png");
                    setTimeout(function () {
                        page.open(system.args[2], function (status3) {
                            if (status3 == 'success') {
                                setTimeout(function () {
                                    var isExist = page.evaluate(function () {
                                        if ($('#site-data-analysis-container').html().length > 0) {
                                            return 1;
                                        }
                                        else {
                                            return 2;
                                        }
                                    });

                                    if (isExist == 1) {
                                        var clipRect = page.evaluate(function () {
                                            return document.querySelector('#site-data-analysis-container').getBoundingClientRect();
                                        });
                                        page.clipRect = {
                                            top: clipRect.top + 3,
                                            left: clipRect.left,
                                            width: clipRect.width,
                                            height: clipRect.height
                                        };
                                        console.log(page.renderBase64());
                                        page.render("../../images/db/" + system.args[3] + ".png");
                                    }
                                    else {
                                        console.log('false');
                                    }

                                    phantom.exit();
                                }, 120000);
                            }
                            else {
                                console.log('Unable to access network: ' + system.args[3]);
                                phantom.exit();
                            }
                        });
                    }, 10000);
                }
            });
        }, 2000);
    }
});



