var page = require('webpage').create(), system = require('system'), fs = require('fs');

var timeoutId = 0;
var waitCounter = 0;
var path = '';
var content = '';

page.viewportSize = { width: 1820, height: 900 };
page.open(system.args[4], function (status) {
    if (status !== 'success') {
        pageUnsuccess();
    }
    else {        
        setTimeout(function () {
            page.open(system.args[5], function (status2) {
                if (status2 !== 'success') {
                    pageUnsuccess();
                }
                else {
                    setTimeout(function () {
                        page.open(system.args[2], function (status3) {
                            if (status3 == 'success') {
                                pageSiteAnalysis(0);
                            }
                            else {
                                pageUnsuccess();
                            }
                        });
                    }, 5000);
                }
            });
        }, 2000);
    }
});

function pageLogout() {
    page.open(system.args[6]);
}

function pageUnsuccess() {
    console.log('Unable to access network');
    phantom.exit();
}

function pageSiteAnalysis(isLoaded) {
    setTimeout(function () {
        var isExist = page.evaluate(function () {
            if ($('#sa-main-container').html().length > 0) {
                return 1;
            }
            else {
                return 2;
            }
        });

        if (isExist == 1) {
            var clipRect = page.evaluate(function () {
                return document.querySelector('#sa-main-container').getBoundingClientRect();
            });
            page.clipRect = {
                top: clipRect.top - 5,
                left: clipRect.left - 5,
                width: clipRect.width + 15,
                height: clipRect.height + 15
            };
            if (isLoaded > 0) {
                setTimeout(function () {
                    page.render("../../images/db/" + system.args[3] + ".png");
                    console.log(page.renderBase64());
                    pageLogout();
                    phantom.exit();
                }, 20000);
            }
            
        }
        else {
            if (waitCounter == 20) {
                console.log(page.renderBase64());
                phantom.exit();
            }
            waitCounter++;
            //page.render("../../images/db/" + system.args[3] + "__" + waitCounter + ".png");
            pageSiteAnalysis(waitCounter);
        }
        
    }, 20000);
}

page.onResourceError = function (resourceError) {
    var path = 'logs/onResourceError/' + system.args[3] + '.txt';
    if (!fs.exists(path))
        fs.write(path, '', 'w');
    fs.write(path, 'RESOURCES ERROR\nReason: ' + resourceError.errorString + '\nUrl: ' + resourceError.url + '\n\n\n', 'a');
};

page.onConsoleMessage = function (msg, lineNum, sourceId) {
    var path = 'logs/onConsoleMessage/' + system.args[3] + '.txt';
    if (!fs.exists(path))
        fs.write(path, '', 'w');
    fs.write(path, 'CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")\n\n\n', 'a');

    if (msg == 'site analysis data cleared') {
        pageSiteAnalysis(1);
        //phantom.exit();
    }
};

page.onError = function (msg, trace) {
    var path = 'logs/onError/' + system.args[3] + '.txt';
    if (!fs.exists(path))
        fs.write(path, '', 'w');

    var msgStack = ['ERROR: ' + msg];

    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function (t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }

    fs.write(path, msgStack.join('\n'), 'a');
    //phantom.exit();
};

page.onLoadStarted = function () {
    var path = 'logs/onLoadStarted/' + system.args[3] + '.txt';
    if (!fs.exists(path))
        fs.write(path, '', 'w');

    var currentUrl = page.evaluate(function () {
        return window.location.href;
    });

    fs.write(path, 'Current page ' + currentUrl + ' will gone...', 'a');
    fs.write(path, 'Now loading a new page...\n\n\n', 'a');
};

page.onLoadFinished = function (status) {
    var path = 'logs/onLoadFinished/' + system.args[3] + '.txt';
    if (!fs.exists(path))
        fs.write(path, '', 'w');

    fs.write(path, 'Status: ' + status + '\n\n\n', 'a');
};