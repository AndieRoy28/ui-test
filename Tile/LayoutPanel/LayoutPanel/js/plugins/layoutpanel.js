﻿define([CURRENT_DOMAIN + "js/libs/knockout.js"],
    function (ko) {
        function LayoutPanel() {
            var self = this;
            self.panelId = '';
            self.targets = [];
            self.behaviour = 'Normal';

            var minWidth = 300;
            var minHeight = 300;
            var margin = 5;
            var scale = 1;


            self.init = function (data) {
                if (data.id != undefined && data.id != null && data.id != '')
                    self.panelId = data.id;
                if (data.behaviour != undefined && data.behaviour != null && data.behaviour != '')
                    self.behaviour = data.behaviour;
                switch (self.behaviour) {
                    case 'Normal':
                        self.updateLayout = self.updateNormalLayout;
                        self.initPositioning = self.NormalInitPositioning;
                        break;
                    case 'LeftTop':
                        self.updateLayout = self.updateLeftTopLayout;
                        self.initPositioning = self.LeftTopInitPositioning;
                        break;
                }
            };

            self.subscribe = function (obj, x, y) {
                //obj.Id()
                //obj.width()
                //obj.height()
                if (obj != undefined && obj != null) {
                    if (obj.layout == undefined) {
                        obj.layout = {
                            col: 0,
                            row: 0,
                            colCount: 1,
                            rowCount: 1
                        };
                        obj.posLeft = 0;
                        obj.posTop = 0;
                        if (x != undefined && x != null)
                            obj.posLeft = x;
                        if (y != undefined && y != null)
                            obj.posTop = y;
                        var col = self.getInt(obj.posLeft / minWidth);
                        var row = self.getInt(obj.posTop / minWidth);
                        obj.col = col;
                        obj.row = row;
                        obj.colCount = 1;
                        obj.rowCount = 1;
                        obj.isFloatItem = false;
                    }
                    obj.initIndex = self.targets.length;
                    if (self.initPositioning != undefined)
                        self.initPositioning(obj);
                    self.targets.push(obj);
                    self.subscribe();
                }
            };

            self.unscribe = function (obj) {
                //for (var i = 0; i < self.targets.length; i++) {
                //    if (self.targets[i].Id() == obj.Id()) {
                //        self.targets[i].layout = null;
                //        self.targets.splice(i, 1);
                //        break;
                //    }
                //}
            };

            self.unscribeAll = function () {
                for (var i = 0; i < self.targets.length; i++) {
                    self.targets[i].layout = null;
                }
                self.targets = [];
            };


            self.getInt = function (v) {
                return parseInt(v.toString());
            };

            self.getNormalPos = function (columns, col, row, colCount, rowCount) {
                var resCol = 0;
                var resRow = 0;
                var sortedColumns = JSON.parse(JSON.stringify(columns)).sort(function (a, b) { return a.row - b.row; });
                for (var i = 0; i < sortedColumns.length; i++) {
                    var colIndex = sortedColumns[i].col;
                    if (colCount == 1) {
                        resCol = colIndex;
                        resRow = columns[colIndex].row;
                        columns[colIndex].row = columns[colIndex].row + rowCount;
                        break;
                    }
                    else if (colCount > 1) {
                        if (columns.length - colIndex >= colCount) {
                            var canAdd = true;
                            resRow = columns[colIndex].row;
                            for (var j = colIndex; j < colIndex + colCount; j++) {
                                if (columns[j].row > resRow) {
                                    canAdd = false;
                                    resRow = columns[j].row;
                                }
                            }
                            if (canAdd == false)
                                colIndex = 0;
                            resCol = colIndex;
                            for (var j = colIndex; j < colIndex + colCount; j++) {
                                columns[j].row = resRow + rowCount;
                            }
                            break;
                        }
                        else {
                            colIndex = 0;
                            resCol = colIndex;
                            resRow = columns[colIndex].row;
                            if (columns.length < colCount)
                                colCount = columns.length;
                            for (var j = colIndex; j < colCount; j++) {
                                if (columns[j].row > resRow) {
                                    resRow = columns[j].row;
                                }
                            }
                            for (var j = colIndex; j < colCount; j++) {
                                columns[j].row = resRow + rowCount;
                            }
                            break;
                        }
                    }
                }
                return { col: resCol, row: resRow };
            }


            //Normal behaviour
            self.updateNormalLayout = function (target, matrix, cellmargin) {
                if (target == undefined) {
                    var sceneContainer = $("#" + self.panelId);
                    if (sceneContainer.length > 0) {
                        var height = 0;
                        var width = sceneContainer.width();
                        for (var i = 0; i < self.targets.length; i++) {
                            d3.select('#' + self.targets[i].Id())
                                .style('top', top + 'px')
                                .style('left', (self.targets[i].col * (minWidth + margin)) + 'px');
                            var container = $('#' + self.targets[i].Id());
                            if (container.length > 0) {
                                var top = self.targets[i].row * (minHeight + margin);
                                var h = top + self.targets[i].initHeight;
                                if (h > height)
                                    height = h;
                                d3.select('#' + self.targets[i].Id()).transition().duration(50)
                                    .style('top', top + 'px')
                                    .style('left', (self.targets[i].col * (minWidth + margin)) + 'px');
                            }

                        }
                        sceneContainer.height(height + 'px');
                    }
                }
                else {
                    if (cellmargin == undefined)
                        cellmargin = 0;
                    console.log('start ' + target.Id() + ' col=' + target.col + ' row=' + target.row + ' colCount=' + target.colCount + ' rowCount='+target.rowCount);
                    var sceneContainer = $("#" + self.panelId);
                    matrix = {};
                    for (var i = 0; i < self.targets.length; i++) {
                        if (self.targets[i].Id() != target.Id()) {
                            self.fillMatrixCell(matrix, self.targets[i], 1);
                        }
                    }
                    console.log('matrix1 = ' + JSON.stringify(matrix));
                    if (sceneContainer.length > 0) {
                        if (!self.checkMatrixCellRange(matrix, target.col, target.colCount, target.row, target.rowCount)) {
                            var floatTargets = [];
                            for (var i = 0; i < self.targets.length; i++) {
                                if (self.targets[i].Id() != target.Id() && (
                                    (self.targets[i].col <= target.col && target.col < self.targets[i].col + self.targets[i].colCount || target.col <= self.targets[i].col && self.targets[i].col < target.col + target.colCount ||
                                    self.targets[i].col < target.col + target.colCount && target.col + target.colCount < self.targets[i].col + self.targets[i].colCount || target.col < self.targets[i].col + self.targets[i].colCount && self.targets[i].col + self.targets[i].colCount < target.col + target.colCount)
                                    &&
                                    (self.targets[i].row <= target.row && target.row < self.targets[i].row + self.targets[i].rowCount || target.row <= self.targets[i].row && self.targets[i].row < target.row + target.rowCount ||
                                    self.targets[i].row < target.row + target.rowCount && target.row + target.rowCount < self.targets[i].row + self.targets[i].rowCount || target.row < self.targets[i].row + self.targets[i].rowCount && self.targets[i].row + self.targets[i].rowCount < target.row + target.rowCount)
                                    ))
                                {
                                    self.targets[i].isFloatItem = true;
                                    floatTargets.push(self.targets[i]);
                                    console.log('float ' + self.targets[i].Id() + ' mc=' + self.targets[i].col + ' mr=' + self.targets[i].row + ' mcC=' + self.targets[i].colCount + ' mrC=' + self.targets[i].rowCount);
                                }
                                else {
                                    self.targets[i].isFloatItem = false;
                                }
                            }
                            matrix = {};
                            for (var i = 0; i < self.targets.length; i++) {
                                if (self.targets[i].isFloatItem == false) {
                                    self.fillMatrixCell(matrix, self.targets[i], 1);
                                }
                            }
                            console.log('matrix2 = ' + JSON.stringify(matrix));
                            console.log('float count=' + floatTargets.length);
                            for (var i = 0; i < floatTargets.length; i++) {
                                var direction = 'down';
                                var downRow = self.moveItemDown(matrix, floatTargets[i], floatTargets[i].row + 1);
                                var dif = Math.abs(downRow - floatTargets[i].row);
                                var leftCol = self.moveItemLeft(matrix, floatTargets[i], floatTargets[i].col - 1);
                                var colDiff = Math.abs(leftCol - floatTargets[i].col);
                                if (dif > colDiff && leftCol >= 0) {
                                    dif = colDiff;
                                    direction = 'left';
                                }
                                var rightCol = self.moveItemRight(matrix, floatTargets[i], floatTargets[i].col + 1);
                                colDiff = Math.abs(rightCol - floatTargets[i].col);
                                if (dif > colDiff) {
                                    dif = colDiff;
                                    direction = 'right';
                                }
                                var topRow = self.moveItemTop(matrix, floatTargets[i], floatTargets[i].row - 1);
                                colDiff = Math.abs(topRow - floatTargets[i].row);
                                if (dif > colDiff) {
                                    dif = colDiff;
                                    direction = 'top';
                                }
                                floatTargets[i].isFloatItem = false;
                                self.fillMatrixCell(matrix, floatTargets[i], 0);
                                switch (direction) {
                                    case 'down':
                                        floatTargets[i].row = downRow;
                                        break;
                                    case 'left':
                                        floatTargets[i].col = leftCol;
                                        break;
                                    case 'right':
                                        floatTargets[i].col = rightCol;
                                        break;
                                    case 'top':
                                        floatTargets[i].row = topRow;
                                        break;
                                }
                                self.fillMatrixCell(matrix, floatTargets[i], 1);
                                var top = floatTargets[i].row * (minHeight + margin);
                                d3.select('#' + floatTargets[i].Id()).transition().duration(50)
                                    .style('top', top + 'px')
                                    .style('left', (floatTargets[i].col * (minWidth + margin)) + 'px');
                            }
                        }
                    }
                }
                console.log('end');
            };

            self.moveItemDown = function (matrix, target, newRow) {
                if (!self.checkMatrixCellRange(matrix, target.col, target.colCount, newRow, target.rowCount)) {
                    newRow = newRow + 1;
                    return self.moveItemDown(matrix, target, newRow);
                }
                else {
                    return newRow;
                }
            };

            self.moveItemTop = function (matrix, target, newRow) {
                if (!self.checkMatrixCellRange(matrix, target.col, target.colCount, newRow, target.rowCount)) {
                    newRow = newRow - 1;
                    if (newRow < 0)
                        return newRow;
                    return self.moveItemTop(matrix, target, newRow);
                }
                else {
                    return newRow;
                }
            };

            self.moveItemLeft = function (matrix, target, newCol) {
                if (!self.checkMatrixCellRange(matrix, newCol, target.colCount, target.row, target.rowCount)) {
                    newCol = newCol - 1;
                    if (newCol < 0)
                        return newCol;
                    return self.moveItemLeft(matrix, target, newCol);
                }
                else {
                    return newCol;
                }
            };

            self.moveItemRight = function (matrix, target, newCol) {
                if (!self.checkMatrixCellRange(matrix, newCol, target.colCount, target.row, target.rowCount)) {
                    newCol = newCol + 1;
                    return self.moveItemRight(matrix, target, newCol);
                }
                else {
                    return newCol;
                }
            };


            self.fillMatrixCell = function (matrix, target, value) {
                if (target.col != undefined && target.row != undefined && target.colCount != undefined && target.rowCount != undefined) {
                    for (var i = target.col; i < target.col + target.colCount; i++) {
                        var column = matrix[i];
                        if (column == undefined) {
                            column = {};
                            matrix[i] = column;
                        }
                        for (var j = target.row; j < target.row + target.rowCount; j++) {
                            column[j] = value;
                        }
                    }
                }
            };

            self.checkMatrixCellRange = function (matrix, col, colCount, row, rowCount) {
                if (col < 0 || row < 0 || colCount < 1 || rowCount < 1)
                    return false;
                var res = true;
                for (var i = col; i < col + colCount; i++) {
                    var column = matrix[i];
                    if (column != undefined) {
                        for (var j = row; j < row + rowCount; j++) {
                            if (column[j] != undefined) {
                                res = false;
                                break;
                            }
                        }
                    }
                    if (res == false)
                        break;
                }
                return res;
            };

            self.NormalInitPositioning = function (target) {
                var draggable = d3.select('#' + target.Id() + ' .tile-header');
                if (draggable.length > 0) {
                    var drag = d3.behavior.drag()
                        .on("drag", function () {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                target.posLeft = target.startLeft - target.startX + d3.event.sourceEvent.pageX;
                                target.posTop = target.startTop - target.startY + d3.event.sourceEvent.pageY;
                                if (target.posTop >= 0 && target.posLeft >= 0) {
                                    container.css('top', target.posTop + 'px');
                                    container.css('left', target.posLeft + 'px');
                                    //console.log(target.Id() + ' left=' + target.posLeft + ' top=' + target.posTop);
                                    //self.onLeftTopChartPositionChange({ id: target.Id(), item: target, left: target.posLeft, top: target.posTop });
                                }
                            }
                        })
                        .on("dragstart", function () {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                target.startLeft = parseFloat(container.css('left').replace('px', ''));
                                target.startTop = parseFloat(container.css('top').replace('px', ''));
                            }
                            target.startX = d3.event.sourceEvent.pageX;
                            target.startY = d3.event.sourceEvent.pageY;
                            d3.event.sourceEvent.stopPropagation();
                        })
                        .on("dragend", function () {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                target.posLeft = Math.round(target.posLeft / minWidth) * (minWidth + margin);
                                target.posTop = Math.round(target.posTop / minHeight) * (minHeight + margin);
                                target.col = self.getInt(target.posLeft / minWidth);
                                target.row = self.getInt(target.posTop / minHeight);
                                if (target.posTop >= 0 && target.posLeft >= 0) {
                                    container.css('top', target.posTop + 'px');
                                    container.css('left', target.posLeft + 'px');
                                    //console.log('dragend ' + target.Id() + ' left=' + target.posLeft + ' top=' + target.posTop);
                                }
                                self.updateNormalLayout(target);
                                //self.onLeftTopChartPositionChange({ id: target.Id(), item: target, left: target.posLeft, top: target.posTop });
                            }
                        });
                    draggable.call(drag)
                }
                if (self.NormalInitResizeCorner != undefined)
                    self.NormalInitResizeCorner(target);
            };

            self.NormalInitResizeCorner = function (target) {
                var draggable = d3.select('#' + target.Id() + ' .tile-resize-corner');
                if (draggable.length > 0) {
                    var drag = d3.behavior.drag()
                        .on("drag", function (d) {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                var draggableWidth = container[0].offsetWidth;
                                var draggableLeft = container[0].offsetLeft;
                                var offset = container.offset();
                                var containerLeft = offset.left;
                                var newWidth = d3.event.sourceEvent.pageX - containerLeft;
                                var containerTop = offset.top;
                                var newHeight = d3.event.sourceEvent.pageY - containerTop;

                                var widthBorder = $('#' + target.Id() + ' .tile-resize-width');
                                var heightBorder = $('#' + target.Id() + ' .tile-resize-height');
                                widthBorder.width(newWidth + 'px');
                                widthBorder.css('top', newHeight + 'px');
                                heightBorder.height(newHeight + 'px');
                                heightBorder.css('left', newWidth + 'px');
                                target.resizeWidth = newWidth;
                                target.resizeHeight = newHeight - 100;
                            }
                        })
                        .on("dragstart", function () {
                            d3.event.sourceEvent.stopPropagation();
                        })
                        .on("dragend", function () {
                            var wCount = Math.round(target.resizeWidth / minWidth);
                            var hCount = Math.round(target.resizeHeight / minHeight);
                            target.resizeWidth = wCount * minWidth + (wCount - 1) * margin;
                            target.resizeHeight = hCount * minHeight + (hCount - 1) * margin;
                            if (target.resizeWidth > 0 && target.resizeHeight > 0) {
                                target.initWidth = target.resizeWidth;
                                target.initHeight = target.resizeHeight;
                                target.width(target.initWidth * scale);
                                target.height(target.initHeight * scale);
                                //console.log('resize colcount=' + target.colCount + '  rowcount=' + target.rowCount);
                                target.colCount = wCount;
                                target.rowCount = hCount;

                                //console.log('resize colcount=' + target.colCount + '  rowcount=' + target.rowCount);

                                var widthBorder = $('#' + target.Id() + ' .tile-resize-width');
                                var heightBorder = $('#' + target.Id() + ' .tile-resize-height');
                                widthBorder.width('0px');
                                heightBorder.height('0px');
                                self.updateNormalLayout(target);
                            }
                        });
                    draggable.call(drag)
                }
            };

            //LeftTop behaviour

            self.currentLayoutUpdateId = null;
            self.updateLeftTopLayout = function (id) {
                //return;
                //console.log('update layout 2');
                self.currentLayoutUpdateId = id;
                if (self.updateLayoutTimer) clearTimeout(self.updateLayoutTimer);
                self.updateLayoutTimer = setTimeout(function () {
                    var id = self.currentLayoutUpdateId;
                    console.log('start ' + id);
                    if (id == undefined)
                        id = '';
                    var columns = [];
                    var sceneContainer = $("#" + self.panelId);
                    if (sceneContainer.length > 0) {
                        var height = 0;
                        var width = sceneContainer.width();
                        var colCount = self.getInt(width / minWidth);
                        for (var i = 0; i < colCount; i++) {
                            columns.push({ row: 0, col: i });
                        }
                        self.targets.sort(function (a, b) {
                            if (a.initIndex > b.initIndex)
                                return 1;
                            else if (a.initIndex < b.initIndex)
                                return -1;
                            return 0;
                        });
                        for (var i = 0; i < self.targets.length; i++) {
                            var col = self.getInt(self.targets[i].posLeft / minWidth);
                            var row = self.getInt(self.targets[i].posTop / minWidth);
                            var colCount = self.getInt(self.targets[i].width() / minWidth);
                            var rowCount = self.getInt(self.targets[i].height() / minHeight);

                            var pos = self.getLeftTopPos(columns, col, row, colCount, rowCount);
                            console.log(self.targets[i].Id() + ' pos=' + JSON.stringify(pos));

                            self.targets[i].col = pos.col;
                            self.targets[i].row = pos.row;
                            self.targets[i].colCount = colCount;
                            self.targets[i].rowCount = rowCount;

                            var container = $('#' + self.targets[i].Id());
                            if (container.length > 0) {
                                var top = pos.row * (minHeight + margin);
                                var h = top + self.targets[i].initHeight;
                                if (h > height)
                                    height = h;
                                if (self.targets[i].Id() != id) {
                                    d3.select('#' + self.targets[i].Id()).transition().duration(50)
                                        .style('top', top + 'px')
                                        .style('left', (pos.col * (minWidth + margin)) + 'px');
                                    //container.css('top', top + 'px');
                                    //container.css('left', (pos.col * (minWidth + margin)) + 'px');
                                }
                            }
                        }
                        sceneContainer.height(height + 'px');
                    }
                    console.log('columns=' + JSON.stringify(columns));
                    console.log('end');
                }, 50);
            };

            self.getLeftTopPos = function (columns, col, row, colCount, rowCount) {
                var resCol = 0;
                var resRow = 0;
                var sortedColumns = JSON.parse(JSON.stringify(columns)).sort(function (a, b) { return a.row - b.row; });
                for (var i = 0; i < sortedColumns.length; i++) {
                    var colIndex = sortedColumns[i].col;
                    if (colCount == 1) {
                        resCol = colIndex;
                        resRow = columns[colIndex].row;
                        columns[colIndex].row = columns[colIndex].row + rowCount;
                        break;
                    }
                    else if (colCount > 1) {
                        if (columns.length - colIndex >= colCount) {
                            var canAdd = true;
                            resRow = columns[colIndex].row;
                            for (var j = colIndex; j < colIndex + colCount; j++) {
                                if (columns[j].row > resRow) {
                                    canAdd = false;
                                    resRow = columns[j].row;
                                }
                            }
                            if (canAdd == false)
                                colIndex = 0;
                            resCol = colIndex;
                            for (var j = colIndex; j < colIndex + colCount; j++) {
                                columns[j].row = resRow + rowCount;
                            }
                            break;
                        }
                        else {
                            colIndex = 0;
                            resCol = colIndex;
                            resRow = columns[colIndex].row;
                            if (columns.length < colCount)
                                colCount = columns.length;
                            for (var j = colIndex; j < colCount; j++) {
                                if (columns[j].row > resRow) {
                                    resRow = columns[j].row;
                                }
                            }
                            for (var j = colIndex; j < colCount; j++) {
                                columns[j].row = resRow + rowCount;
                            }
                            break;
                        }
                    }
                }
                return { col: resCol, row: resRow };
            }

            self.onLeftTopChartPositionChange = function (data) {
                var currDimension = data.item;
                var index = currDimension.initIndex;
                var left = data.left;
                var top = data.top;

                var col = self.getInt(left / minWidth);
                var row = self.getInt(top / minWidth);

                if (currDimension.col == col && currDimension.row == row)
                    return;

                var colCount = self.getInt(currDimension.initWidth / minWidth);
                var rowCount = self.getInt(currDimension.initHeight / minHeight);

                console.log('col=' + col + ' row=' + row);

                self.targets.sort(function (a, b) {
                    if (a.initIndex > b.initIndex)
                        return 1;
                    else if (a.initIndex < b.initIndex)
                        return -1;
                    return 0;
                });
                var moveIndex = -1;
                for (var i = 0; i < self.targets.length; i++) {
                    if (self.targets[i].col <= col && col < self.targets[i].col + self.targets[i].colCount && self.targets[i].row <= row && row < self.targets[i].row + self.targets[i].rowCount) {
                        moveIndex = self.targets[i].initIndex;
                        console.log(self.targets[i].Id() + ' item colcount=' + self.targets[i].colCount + '  rowcount=' + self.targets[i].rowCount);
                        break;
                    }
                }
                if (moveIndex < 0)
                    moveIndex = self.targets.length - 1;
                if (moveIndex < index) {
                    for (var i = 0; i < self.targets.length - 1; i++) {
                        if (self.targets[i].initIndex >= moveIndex && self.targets[i].initIndex <= index) {
                            self.targets[i].initIndex = self.targets[i + 1].initIndex;
                        }
                    }
                }
                else {
                    for (var i = self.targets.length - 1; i > 0; i--) {
                        if (self.targets[i].initIndex >= index && self.targets[i].initIndex <= moveIndex) {
                            self.targets[i].initIndex = self.targets[i - 1].initIndex;
                        }
                    }
                }
                console.log('moveIndex=' + moveIndex);
                currDimension.initIndex = moveIndex;
                self.updateLeftTopLayout(data.id);
            };

            self.LeftTopInitPositioning = function (target) {
                var draggable = d3.select('#' + target.Id() + ' .tile-header');
                if (draggable.length > 0) {
                    var drag = d3.behavior.drag()
                        .on("drag", function () {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                target.posLeft = target.startLeft - target.startX + d3.event.sourceEvent.pageX;
                                target.posTop = target.startTop - target.startY + d3.event.sourceEvent.pageY;
                                if (target.posTop >= 0 && target.posLeft >= 0) {
                                    container.css('top', target.posTop + 'px');
                                    container.css('left', target.posLeft + 'px');
                                    console.log(target.Id() + ' left=' + target.posLeft + ' top=' + target.posTop);
                                    self.onLeftTopChartPositionChange({ id: target.Id(), item: target, left: target.posLeft, top: target.posTop });
                                }
                            }
                        })
                        .on("dragstart", function () {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                target.startLeft = parseFloat(container.css('left').replace('px', ''));
                                target.startTop = parseFloat(container.css('top').replace('px', ''));
                            }
                            target.startX = d3.event.sourceEvent.pageX;
                            target.startY = d3.event.sourceEvent.pageY;
                            d3.event.sourceEvent.stopPropagation();
                        })
                        .on("dragend", function () {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                target.posLeft = Math.round(target.posLeft / minWidth) * (minWidth + margin);
                                target.posTop = Math.round(target.posTop / minHeight) * (minHeight + margin);
                                if (target.posTop >= 0 && target.posLeft >= 0) {
                                    container.css('top', target.posTop + 'px');
                                    container.css('left', target.posLeft + 'px');
                                    //console.log('dragend ' + target.Id() + ' left=' + target.posLeft + ' top=' + target.posTop);
                                }
                                self.onLeftTopChartPositionChange({ id: target.Id(), item: target, left: target.posLeft, top: target.posTop });
                            }
                        });
                    draggable.call(drag)
                }
                if (self.LeftTopInitResizeCorner != undefined)
                    self.LeftTopInitResizeCorner(target);
            };

            self.LeftTopInitResizeCorner = function (target) {
                var draggable = d3.select('#' + target.Id() + ' .tile-resize-corner');
                if (draggable.length > 0) {
                    var drag = d3.behavior.drag()
                        .on("drag", function (d) {
                            var container = $('#' + target.Id());
                            if (container.length > 0) {
                                var draggableWidth = container[0].offsetWidth;
                                var draggableLeft = container[0].offsetLeft;
                                var offset = container.offset();
                                var containerLeft = offset.left;
                                var newWidth = d3.event.sourceEvent.pageX - containerLeft;
                                var containerTop = offset.top;
                                var newHeight = d3.event.sourceEvent.pageY - containerTop;

                                var widthBorder = $('#' + target.Id() + ' .tile-resize-width');
                                var heightBorder = $('#' + target.Id() + ' .tile-resize-height');
                                widthBorder.width(newWidth + 'px');
                                widthBorder.css('top', newHeight + 'px');
                                heightBorder.height(newHeight + 'px');
                                heightBorder.css('left', newWidth + 'px');
                                target.resizeWidth = newWidth;
                                target.resizeHeight = newHeight - 100;
                            }
                        })
                        .on("dragstart", function () {
                            d3.event.sourceEvent.stopPropagation();
                        })
                        .on("dragend", function () {
                            var wCount = Math.round(target.resizeWidth / minWidth);
                            var hCount = Math.round(target.resizeHeight / minHeight);
                            target.resizeWidth = wCount * minWidth + (wCount - 1) * margin;
                            target.resizeHeight = hCount * minHeight + (hCount - 1) * margin;
                            if (target.resizeWidth > 0 && target.resizeHeight > 0) {
                                target.initWidth = target.resizeWidth;
                                target.initHeight = target.resizeHeight;
                                target.width(target.initWidth * scale);
                                target.height(target.initHeight * scale);
                                console.log('resize colcount=' + target.colCount + '  rowcount=' + target.rowCount);
                                target.colCount = wCount;
                                target.rowCount = hCount;

                                console.log('resize colcount=' + target.colCount + '  rowcount=' + target.rowCount);

                                var widthBorder = $('#' + target.Id() + ' .tile-resize-width');
                                var heightBorder = $('#' + target.Id() + ' .tile-resize-height');
                                widthBorder.width('0px');
                                heightBorder.height('0px');
                                self.updateLeftTopLayout(target.Id());
                            }
                        });
                    draggable.call(drag)
                }
            };

        };

        return (LayoutPanel);
    });