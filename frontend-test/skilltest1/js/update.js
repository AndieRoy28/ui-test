const id = new URLSearchParams(window.location.search).get('id');
const form = document.querySelector('form');
const _d = new Date(Date.UTC(2012, 11, 20, 3, 0, 0));
const d = _d.toLocaleString('en-GB', { timeZone: 'UTC' });

const url = 'http://localhost:3001';
const uri = url + '/recipes/' + id;
const folder = '/skilltest1';

const updateRecipe = async (e) =>  {
    e.preventDefault();

    const r = {
        uuid: id,
        title: form.title.value,
        description: form.description.value,
        images: JSON.parse(form.images.value),
        servings: form.servings.value,
        prepTime: form.prepTime.value,
        cookTime: form.cookTime.value,
        postDate: form.postDate.value,
        editDate: d,
        ingredients: ((form.ingredients.value == '' ) ? [] : JSON.parse(form.ingredients.value)),
        directions: ((form.directions.value == '' ) ? [] : JSON.parse(form.directions.value))
    }

    await fetch(uri, {
        method: 'PUT',
        body: JSON.stringify(r),
        headers: { 'Content-Type': 'application/json'}
    }).then(response => {
        window.location.replace('/skilltest1/index.html');
    });
    
}

form.addEventListener('submit', updateRecipe);

var jsondata = { "line": ["", "33", "10"] };
    

    const renderFormValue = async () => {
        const res = await fetch(uri);
        const recipe = await res.json();
        
        for (key in recipe) {
            if (recipe.hasOwnProperty(key)) {
                console.log('key >>> ' + key);
                console.log('val >>> ' + recipe[key]);

                console.log('type >>> ' + (typeof recipe[key] == 'string'));

                if(form.querySelector('input[name="'+ key +'"]') !== null){                    

                    if(recipe[key] >= 0 || typeof recipe[key] == 'string'){
                        form.querySelector('input[name="'+ key +'"]').value = recipe[key];
                    }
                    else{
                        form.querySelector('input[name="'+ key +'"]').value = JSON.stringify(recipe[key]);
                    }
                }
                else if(form.querySelector('textarea[name="'+ key +'"]') !== null){
                    form.querySelector('textarea[name="'+ key +'"]').value = recipe[key];

                }
            }
        }

    };

window.addEventListener('DOMContentLoaded', () => renderFormValue());