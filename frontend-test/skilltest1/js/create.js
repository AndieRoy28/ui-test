function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

const form = document.querySelector('form');
var id = create_UUID();
const _d = new Date(Date.UTC(2012, 11, 20, 3, 0, 0));
var d = _d.toLocaleString('en-GB', { timeZone: 'UTC' })

const createRecipe = async (e) =>  {
    e.preventDefault();

    const r = {
        uuid: id,
        title: form.title.value,
        description: form.description.value,
        images: {
            full: "/img/no_image.jpg",
            "medium": "/img/no_image.jpg",
            "small": "/img/no_image.jpg"
        },
        servings: form.servings.value,
        prepTime: form.prepTime.value,
        cookTime: form.cookTime.value,
        postDate: d,
        editDate: d,
        ingredients: [],
        directions: []
    }

    let url = 'http://localhost:3001';
    let uri = url + '/recipes';
    let folder = '/skilltest1';

    await fetch(uri, {
        method: 'POST',
        body: JSON.stringify(r),
        headers: { 'Content-Type': 'application/json'}
    }).then(response => {
        window.location.replace('/skilltest1/index.html');
    });
    
}

form.addEventListener('submit', createRecipe);