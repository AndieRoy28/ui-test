const container = document.querySelector('.recipe_container');




const renderRecipes = async () => {
    let url = 'http://localhost:3001';
    let uri = url + '/recipes';
    let folder = '/skilltest1';

    const res = await fetch(uri);
    const recipes = await res.json();

    //console.log(recipes);
    let template = '';
    recipes.forEach(recipe => {
        template += `
             <div class="recipe">
             <img src="${url}${recipe.images.small}" alt="${recipe.description}" title="${recipe.title}">
                <h2>${recipe.title}</h2>
                <p>${recipe.description}</p>
                <a class="link2" href="${folder}/update.html?id=${recipe.uuid}">update</a> 
                <a class="link3" href="${folder}/details.html?id=${recipe.uuid}">read more</a>
             </div>
             `;
    });
    
    container.innerHTML = template;

}

window.addEventListener('DOMContentLoaded', () => renderRecipes());