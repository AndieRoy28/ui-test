const url = 'http://localhost:3001';
const folder = '/skilltest1';
const id = new URLSearchParams(window.location.search).get('id');
const uri = url + '/recipes/' + id;
let listSpecial = [];

const container = document.querySelector('.recipe_container');
const deleteBtn = document.querySelector('.delete');

async function fetchDetails() {
    const response = await fetch(url + '/recipes/' + id);
    const detail = await response.json();
    let ingredientIds = [];

    detail.ingredients.forEach(i => {
        ingredientIds.push(i.uuid);
    });

    console.log(ingredientIds);

    //Get Ingredients with a matching ingredientId listed in the specials
    let requestsSpecial = ingredientIds.map(i => fetch(url +'/specials?ingredientId='+ i));
    Promise.all(requestsSpecial).then(responses => {
        return responses;
    }).then(responses => Promise.all(responses.map(r => r.json())))
        .then(specials => {specials.forEach(s => {
            if(s[0] != null) {
                for (ss = 0; ss < s.length; ss++) {
                    listSpecial.push(s[ss]);
                }            
                console.log(listSpecial);
            }
        });

        //render template here
        console.log('listSpecial');

        //here show the Ingredient and special
        let templateIngredients = '<div class="ingredients"><table><thead><tr><th>Name</th><th>Amount</th><th>Measurement</th></tr></thead><tbody>';
        detail.ingredients.forEach(i => {
            let specialText = '';
            for (l = 0; l < listSpecial.length; l++) {
                if(listSpecial[l].ingredientId == i.uuid){
                    specialText += `
                        <div class="special-content">${listSpecial[l].type} : ${listSpecial[l].title} - ${listSpecial[l].text} </div>
                    `;
                }
            }  
            templateIngredients += `
                <tr>
                    <td><div class="ingredient-name-content">${i.name} </div> ${specialText} </td>
                    <td>${i.amount}</td>
                    <td>${i.measurement}</td>
                </tr>             
                `;
        });

        templateIngredients += '</tbody></table></div>';

        //show Instruction for the recipe
        let templateDirections = '<ul class="directions">';
        detail.directions.forEach(d => {
            templateDirections += `<li>${d.instructions}</li>`;
        });

        templateDirections += '</ul>';

        let template = `
            <div class="recipe_content">
                <img src="${url}/${detail.images.medium}" alt="${detail.description}" title="${detail.title}">
            <h2>${detail.title}</h2>
            <p class="p_date">${detail.postDate}</p>
            <p>${detail.description}</p>

            <table><thead><tr><th>Servings</th><th>Preparation Time</th><th>Cook Time</th></tr></thead><tbody>
            <tr>
                <td>${detail.servings} </td>
                <td>${detail.prepTime} </td>
                <td>${detail.cookTime} </td>
            </tr>  
            </tbody></table>
            <br><br>

            <a class="link1" href="${folder}/update.html?id=${detail.uuid}">update</a>

            <div class="line"></div>
            <div class="some_container">
                <h2 class="title1">Ingredients</h2>
                ${templateIngredients}
            </div>
            <div class="line"></div>
            <div class="some_container">
                <h2 class="title2">Instruction</h2>
                ${templateDirections}
            </div>
            </div>
        `;
        container.innerHTML = template;

        deleteBtn.addEventListener('click', async (e) =>{
            const res = await fetch(uri, {
                method: 'DELETE'
            })
            window.location.replace('/skilltest1/index.html');
        });
    });
};

fetchDetails().then(r => r);